{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "# Versions-Kontrolle: Git\n",
    "\n",
    "Die Hauptidee von Versions-Kontrolle ist es, die Gesamtheit der Änderungen eines Software-Projekts im Laufe der Zeit zu verwalten und zu speichern. Ganz simpel kann man das durch \"regelmäßiges Kopieren\" von Hand machen. Z.B. kann man einen Projekt-Ordner\n",
    "\n",
    "```bash\n",
    "$ ls project/\n",
    "src/\n",
    "lib/\n",
    "doc/\n",
    "test/\n",
    "main.py\n",
    "README.md\n",
    ".dir-locals.el\n",
    "```\n",
    "\n",
    "regelmäßig in ein spezielles Verzeichnis kopieren und geeignet umbenennen:\n",
    "\n",
    "```bash\n",
    "$ ls project-history/\n",
    "2016-09-09 Init/\n",
    "2016-09-10 Add frobnicate method/\n",
    "2016-09-12 Did stuff/\n",
    "2016-09-13 Add tests/\n",
    "```\n",
    "\n",
    "[![](https://imgs.xkcd.com/comics/iso_8601.png)](https://xkcd.com/1179/)\n",
    "\n",
    "Das Grund-Konzept von Git ist recht ähnlich zu diesem Ansatz, liefert aber darauf aufbauend erheblich viel mehr Funktionalität.\n",
    "\n",
    "Haupt-Anwengungsfälle von Versions-Kontrolle sind:\n",
    "\n",
    "- Kollaboratives Arbeiten an einem Projekt.\n",
    "- Recovery im Ernstfall.\n",
    "\n",
    "(Beide haben tatsächlich erstaunlich viel miteinander zu tun.)\n",
    "\n",
    "## Versions-Kontroll-Systeme\n",
    "\n",
    "Vor einigen Jahren gab es eine größere Zahl von Versions-Kontroll-Systemen. Mittlerweile sind nur noch zwei davon relevant:\n",
    "\n",
    "- **Subversion (SVN):** Der \"alte\" Standard; existiert vor allem in alten Projekten, verliert aber immer mehr an Relevanz.\n",
    "- **Git:** Entwickelt für den Linux-Kernel und mittlerweile *de facto* Standard im Open-Source-Bereich.\n",
    "\n",
    "Git kann prinzipiell für beliebige Daten genutzt werden, hat aber Probleme mit großen Binär-Dateien (Medien, Office etc). Am besten ist es für Text-Dateien (Code, LaTeX etc) geeignet.\n",
    "\n",
    "## Konzepte\n",
    "\n",
    "[![](https://imgs.xkcd.com/comics/git.png)](https://xkcd.com/1597/)\n",
    "\n",
    "### Commits\n",
    "\n",
    "Git speicher im Prinzip *Snapshots* des Codes zu bestimmten Zeitpunkten. Ein solcher Snapshot heißt **Commit**. Ein Commit enthält\n",
    "\n",
    "- Metadaten: Autor, Datum, eine *Commit Message*, ...\n",
    "- (Eine Referenz auf) Einen Verzeichnisbaum, d.h. eine Sammlung von Dateien und Verzeichnissen, die *Gesamtheit* des Codes zu diesem Zeitpunkt. (Anders als SVN verwaltet Git nicht verschiedene Dateien unabhängig voneinander.)\n",
    "- Eine beliebige Anzahl von Vorgänger-Commits. Der Vorgänger eines Commits ist nicht notwendigerweise derjenige, der zeitlich direkt davor lag, sondern derjenige, der zur Erzeugung der jeweiligen Version als Basis verwendet wurde.\n",
    "\n",
    "Die meisten Repositories haben genau einen Commit ohne Vorgänger (der erste). Commits mit mehr als einem Vorgänger nennt man *Merge*.\n",
    "\n",
    "Ein Commit kann, nachdem er erzeugt wurde, nicht mehr geändert werden.\n",
    "\n",
    "### Branches\n",
    "\n",
    "Commits können sowohl mehrere Vorgänger als auch mehrere Nachfolger haben:\n",
    "\n",
    "![](../images/git/dag.svg)\n",
    "\n",
    "Dadurch können Verläufe entstehen, in denen es mehrere \"aktuelle\" Commits gibt. Dieses Aufspalten der Entwicklung nennt man **Branches**. Es gibt Methoden, um zwischen diesen Branches hin- und herzuwechseln (s.u.).\n",
    "\n",
    "#### Warum sind Branches interessant?\n",
    "\n",
    "- Man kann an mehreren unterschiedlichen *Features* arbeiten, ohne dass sich die Änderungen gegenseitig beeinflussen. Beispielsweise kann man in einem Branch zeitweise nicht funktionierenden oder schlecht getesteten Code haben, und in einem anderen mit dem stabilen, älteren Code weiter arbeiten.\n",
    "\n",
    "- Vor allem aber: mehrere Leute können gleichzeitig an einem Projekt arbeiten.\n",
    "\n",
    "### Refs\n",
    "\n",
    "Commits sind *nicht veränderlich*. Man kann in Git neue Commits erzeugen, aber vorhandene nicht mehr ändern oder löschen (jedenfalls nicht ohne großen Aufwand).\n",
    "\n",
    "**Refs** sind dagegen die veränderlichen Teile von Git. Sie sind am ehesten mit Variablen (Bezeichnern) in Python vergleichbar: Eine Ref ist ein Name für einen Commit.\n",
    "\n",
    "![](../images/git/refs.svg)\n",
    "\n",
    "Üblicherweise hat man pro Branch eine *Ref*, die auf den aktuellen Commit des Branches zeigt, und identifiziert Begrifflich Branches mit den entsprechenden *Refs*; man spricht z.B. vom `master`-Branch.\n",
    "\n",
    "Ausserdem gibt es eine *Ref* namens **HEAD**, die üblicherweise auf die *Ref* des aktuellen Branchs zeigt.\n",
    "\n",
    "#### Erzeugen eines neuen Commits in einem Branch\n",
    "\n",
    "Wird in dieser Situation ein neuer Commit im Branch `master` erzeugt, so referenziert die entsprechende *Ref* danach den neuen Commit.\n",
    "\n",
    "![](../images/git/new-commit.svg)\n",
    "\n",
    "### Merges\n",
    "\n",
    "Man kann innerhalb eines Branches die Änderungen eines anderen Branches übernehmen. Dabei wird ein neuer Commit erzeugt, der alle Änderungen enthält und der die aktuellen Commits beider Branches als Vorgänger enthält.\n",
    "\n",
    "![](../images/git/merge1.svg)\n",
    "\n",
    "Wichtig ist dabei, dass Git sich so \"merkt\", wann Merges passieren. Beim zweiten Merge müssen hier z.B. nur noch die Änderungen von `I` und `J` übernommen werden, aber nicht die von `D` und `E` (diese sind schon im ersten Merge `G` enthalten):\n",
    "\n",
    "![](../images/git/merge2.svg)\n",
    "\n",
    "Gits Merge-Algorithmus ist oft sehr gut in der Lage, alle Änderungen automatisch zusammenzuführen, was wesentlich an der detaillierten Kenntnis über die \"Geschichte\" des Codes liegt.\n",
    "\n",
    "## Anwendungs-Beispiele\n",
    "\n",
    "Wir verwenden hier Git auf der Kommandozeile. Alle Befehle sind als \"Unterbefehle\" von `git` verügbar, also z.B. `git init`, `git clone`, etc.\n",
    "\n",
    "### Repository\n",
    "\n",
    "Die Gesamtheit der Commits für ein Git-Projekt nennt man **Repository**. Es gibt zwei wesentliche Wege, ein Repository zu erzeugen.\n",
    "\n",
    "#### `init`\n",
    "\n",
    "`git init` in einem Verzeichnis mit bisher nicht versioniertem Code oder einem leeren Verzeichnis erzeugt ein neues Repository, das bisher keine Commits enthält.\n",
    "\n",
    "#### `clone`\n",
    "\n",
    "Häufiger verwendet man `git clone`. Damit kann ein existierendes Repository in ein neues Verzeichnis kopiert werden.\n",
    "\n",
    "Das Ursprungs-Repository liegt dabei oft auf einem Server (Github, Gitlab, ...). Allerdings ist Git (im Gegensatz zu SVN) **dezentral**, d.h. es muss keinen zentralen Server o.ä. geben. Jedes Repository enthält alle Informationen, und jedes Repository kann geklont werden.\n",
    "\n",
    "Tatsächlich ist es oft so, dass man trotzdem ein zentrales Setup verwendet, und einige Details funktionieren bei einem voll dezentralen Setup anders. Wir simulieren hier (ohne weitere Erklärung) ein zentrales Setup:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Initialized empty Git repository in /home/cruegge/central/\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "mkdir -p ~/central\n",
    "cd ~/central\n",
    "git --bare init"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "In `~/central` liegt jetzt ein Repository, dass sich äquivalent verhält wie Repositories auf zentralen Servern. Dieses kann man nun in ein anderes Verzeichnis *klonen*, z.B. `~/clone1`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Cloning into '/home/cruegge/clone1'...\n",
      "warning: You appear to have cloned an empty repository.\n",
      "done.\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "git clone ~/central ~/clone1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "### Erzeugen von Commits\n",
    "\n",
    "#### Struktur eines Repositories\n",
    "\n",
    "Repositories bestehen aus drei \"Ebenen\":\n",
    "\n",
    "- Der **Worktree** enthält die Dateien, an denen aktuell gearbeitet wird. Nach einem `git clone` enthält der Worktree üblicherweise alle Dateien des aktuellen Commits des `master`-Branches. Alle Datein und Verzeichnisse ausser dem Unterverzeichnis `.git` gehören zum Worktree."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      ".\n",
      "..\n",
      ".git\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "ls -a ~/clone1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "- Der **Index** (auch **Staging Area**) enthält alle Änderungen, die im nächsten Commit enthalten sein sollen. (Oft will man nicht alle Änderungen im Worktree auch [committen](http://www.duden.de/rechtschreibung/committen).)\n",
    "\n",
    "- Ausserdem enthält das Repository alle Commits und Refs wie oben beschrieben (und noch ein paar weitere Dinge).\n",
    "\n",
    "Sowohl der Index als auch Commits etc. liegen im Unterverzeichnis `.git`, das man i.A. nicht direkt bearbeiten sollte.\n",
    "\n",
    "#### `status`\n",
    "\n",
    "`git status` gibt eine Übersicht über die aktuellen Änderung im Worktree und Index:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "On branch master\n",
      "\n",
      "Initial commit\n",
      "\n",
      "Untracked files:\n",
      "  (use \"git add <file>...\" to include in what will be committed)\n",
      "\n",
      "\tmain.py\n",
      "\n",
      "nothing added to commit but untracked files present (use \"git add\" to track)\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone1\n",
    "echo \"print('Hello world!')\" > main.py\n",
    "git status"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "- `main.py` ist hier \"untracked\", da der aktuelle Commit keine Datei diesen Namens enthält (da es gerade noch keinen aktuellen Commit gibt).\n",
    "\n",
    "- Der aktuelle Branch ist `master`, der Standard-Branch in Git.\n",
    "\n",
    "#### `add`\n",
    "\n",
    "Mit `git add` werden Dateien aus dem Worktree in den Index aufgenommen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "On branch master\n",
      "\n",
      "Initial commit\n",
      "\n",
      "Changes to be committed:\n",
      "  (use \"git rm --cached <file>...\" to unstage)\n",
      "\n",
      "\tnew file:   main.py\n",
      "\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone1\n",
    "git add main.py\n",
    "git status"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "#### `commit`\n",
    "\n",
    "Mit `git commit` wird aus dem Index ein neuer Commit erzeugt. Die *Commit Message* kann dabei über die Option `-m` angegeben werden. Andernfalls öffnet sich ein 80er-Jahre Text-Editor (meist `nano` oder `vi`), in dem man nochmal sehen kann, was der Commit enthält, und eine Nachricht schreiben kann."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[master (root-commit) 151a428] Initialer Commit: Hello world!\n",
      " 1 file changed, 1 insertion(+)\n",
      " create mode 100644 main.py\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone1\n",
    "git commit -m \"Initialer Commit: Hello world!\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "- Bei der ersten Verwendung könnte sich Git beschweren, dass es Namen und Email-Adresse des Nutzers wissen will. In dem Fall wird eine Nachricht ausgegeben, die erklärt, wie man die entsprechenden Einstellungen vornehmen kann."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "On branch master\n",
      "Your branch is based on 'origin/master', but the upstream is gone.\n",
      "  (use \"git branch --unset-upstream\" to fixup)\n",
      "nothing to commit, working directory clean\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone1\n",
    "git status"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "#### `log`\n",
    "\n",
    "Mit `git log` kann man die History des aktuellen Branchs anzeigen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "commit 151a4280e3574fdba1c35d66b709998b01e4ee8b\n",
      "Author: Christoph Ruegge <c.ruegge@math.uni-goettingen.de>\n",
      "Date:   Tue Sep 13 17:13:17 2016 +0200\n",
      "\n",
      "    Initialer Commit: Hello world!\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone1\n",
    "git log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "Wir haben nun die folgende Situation:\n",
    "\n",
    "<figure style=\"text-align: center\">\n",
    "<img src=\"../images/git/init.svg\"/>\n",
    "<figcaption>clone1</figcaption>\n",
    "</figure>\n",
    "\n",
    "### Kooperation\n",
    "\n",
    "#### Remotes\n",
    "\n",
    "- Git-Repositories haben ein Konzept von **Remote Repositories**, also Repositories auf ggf. anderen Rechnern, zu denen Commits übertragen oder von denen Commits heruntergeladen werden können.\n",
    "- Standardmäßig konfiguriert `git clone` das Ursprungs-Repository, von dem geklont wurde, als Remote mit dem Namen `origin`.\n",
    "- Repositories können mehrere Remotes haben, meist ist `origin` aber ausreichend.\n",
    "\n",
    "#### `push`\n",
    "\n",
    "Mit `git push` werden die lokalen Änderungen zum Remote übertragen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "To /home/cruegge/central\n",
      " * [new branch]      master -> master\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone1\n",
    "git push"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "- Hier wurde in `~/central` ein neuer Branch `master` erzeugt (vorher war das Repository leer).\n",
    "\n",
    "<figure style=\"text-align: center\">\n",
    "<img src=\"../images/git/init.svg\"/>\n",
    "<figcaption>central</figcaption>\n",
    "</figure>\n",
    "\n",
    "- Beim nächsten `clone` des zentralen Repos wird der Commit heruntergeladen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Cloning into '/home/cruegge/clone2'...\n",
      "done.\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "git clone ~/central ~/clone2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "commit 151a4280e3574fdba1c35d66b709998b01e4ee8b\n",
      "Author: Christoph Ruegge <c.ruegge@math.uni-goettingen.de>\n",
      "Date:   Tue Sep 13 17:13:17 2016 +0200\n",
      "\n",
      "    Initialer Commit: Hello world!\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone2\n",
    "git log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "<figure style=\"text-align: center\">\n",
    "<img src=\"../images/git/init.svg\"/>\n",
    "<figcaption>clone2</figcaption>\n",
    "</figure>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "#### Konflikte\n",
    "\n",
    "Erstellen wir einen neuen Commit in `clone1` und pushen diesen:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[master c8249b2] Be more friendly.\n",
      " 1 file changed, 1 insertion(+)\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "To /home/cruegge/central\n",
      "   151a428..c8249b2  master -> master\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone1\n",
    "echo \"print('Have a nice day!')\" >> main.py\n",
    "git add main.py\n",
    "git commit -m 'Be more friendly.'\n",
    "git push"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "Sowohl `clone1` als auch `central` sehen nun so aus:\n",
    "\n",
    "<figure style=\"text-align: center\">\n",
    "<img src=\"../images/git/second.svg\"/>\n",
    "<figcaption>clone1 &amp; central</figcaption>\n",
    "</figure>\n",
    "\n",
    "Erstellen wir auch einen Commit in `clone2`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[master 04179b3] Be less friendly.\n",
      " 1 file changed, 1 insertion(+)\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "To /home/cruegge/central\n",
      " ! [rejected]        master -> master (fetch first)\n",
      "error: failed to push some refs to '/home/cruegge/central'\n",
      "hint: Updates were rejected because the remote contains work that you do\n",
      "hint: not have locally. This is usually caused by another repository pushing\n",
      "hint: to the same ref. You may want to first integrate the remote changes\n",
      "hint: (e.g., 'git pull ...') before pushing again.\n",
      "hint: See the 'Note about fast-forwards' in 'git push --help' for details.\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone2\n",
    "echo \"print('Hello yourself!')\" >> main.py\n",
    "git add main.py\n",
    "git commit -m 'Be less friendly.'\n",
    "git push"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "`clone2` sieht nun so aus:\n",
    "\n",
    "<figure style=\"text-align: center\">\n",
    "<img src=\"../images/git/second2.svg\"/>\n",
    "<figcaption>clone2</figcaption>\n",
    "</figure>\n",
    "\n",
    "Allerdings wurde beim `push` die Änderung nicht akzeptiert, da dadurch Commit `B` in `central` verloren gehen würde. Dieser Konflikt muss in `clone2` lokal gelöst werden, bevor wieder gepusht werden kann.\n",
    "\n",
    "#### `fetch`\n",
    "\n",
    "Mit `git fetch` werden Commits und Refs vom Remote abgeholt. Die lokalen Refs werden dabei nicht geändert. Stattdessen werden die abgeholten Refs als Branches behandelt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "From /home/cruegge/central\n",
      "   151a428..c8249b2  master     -> origin/master\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone2\n",
    "git fetch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "`clone2` sieht nun so aus:\n",
    "\n",
    "<figure style=\"text-align: center\">\n",
    "<img src=\"../images/git/fetch.svg\"/>\n",
    "<figcaption>clone2</figcaption>\n",
    "</figure>\n",
    "\n",
    "`origin/master` bezeichnet immer den letzten bekannten Zustand des Branchs `master` im Remote `origin` (d.h. hier in `~/central`).\n",
    "\n",
    "#### `merge`\n",
    "\n",
    "Mit `git merge` kann man einen anderen Branch in den aktuellen mergen. Den Branch kann man explizit angeben, standardmäßig wird aber der Remote des aktuellen Branchs verwendet.\n",
    "\n",
    "Beim mergen können Konflikte auftreten, wenn beide Branches die gleiche Stelle einer Datei ändern."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Auto-merging main.py\n",
      "CONFLICT (content): Merge conflict in main.py\n",
      "Automatic merge failed; fix conflicts and then commit the result.\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone2\n",
    "git merge"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "Hier ist ein Konflikt aufgetreten. `git status` zeigt Information darüber an:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "On branch master\n",
      "Your branch and 'origin/master' have diverged,\n",
      "and have 1 and 1 different commit each, respectively.\n",
      "  (use \"git pull\" to merge the remote branch into yours)\n",
      "You have unmerged paths.\n",
      "  (fix conflicts and run \"git commit\")\n",
      "\n",
      "Unmerged paths:\n",
      "  (use \"git add <file>...\" to mark resolution)\n",
      "\n",
      "\tboth modified:   main.py\n",
      "\n",
      "no changes added to commit (use \"git add\" and/or \"git commit -a\")\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone2\n",
    "git status"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "print('Hello world!')\n",
      "<<<<<<< HEAD\n",
      "print('Hello yourself!')\n",
      "=======\n",
      "print('Have a nice day!')\n",
      ">>>>>>> refs/remotes/origin/master\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone2\n",
    "cat main.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "Der Zeile zwischen\n",
    "\n",
    "```\n",
    "<<<<<<< HEAD\n",
    "```\n",
    "\n",
    "und\n",
    "\n",
    "```\n",
    "=======\n",
    "```\n",
    "\n",
    "ist die lokale Änderung, die danach bis\n",
    "\n",
    "```\n",
    ">>>>>>> refs/remotes/origin/master\n",
    "```\n",
    "\n",
    "ist die Änderung im Branch `origin/master`. An dieser Stelle muss man entscheiden, wie die Änderungen \"sinnvoll\" zusammengeführt werden können.\n",
    "\n",
    "Einen Merge kann man Abbrechen mit\n",
    "```bash\n",
    "git merge --abort\n",
    "```\n",
    "\n",
    "Zum Beheben von Merge-Konflikten gibt es spezielle Tools, wie z.B. `meld`. Diese lassen sich mit\n",
    "```bash\n",
    "git mergetool\n",
    "```\n",
    "starten.\n",
    "\n",
    "Übernehmen wir hier der Einfachheit halber die Version aus `origin`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "print('Hello world!')\n",
      "print('Have a nice day!')\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone2\n",
    "sed -i '/<<<</,/====/d; />>>>/d' main.py\n",
    "cat main.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[master baef6b0] Fixed merge conflict\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone2\n",
    "git add main.py\n",
    "git commit -m 'Fixed merge conflict'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "`clone2` sieht nun so aus:\n",
    "\n",
    "<figure style=\"text-align: center\">\n",
    "<img src=\"../images/git/merge.svg\"/>\n",
    "<figcaption>clone2</figcaption>\n",
    "</figure>\n",
    "\n",
    "Jetzt sollte ein `push` wieder möglich sein (es sein denn, in der Zwischenzeit hätte sich etwas Weiteres in `~/central` geändert)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "To /home/cruegge/central\n",
      "   c8249b2..baef6b0  master -> master\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone2\n",
    "git push"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "`clone2` und `central` sehen nun so aus:\n",
    "\n",
    "<figure style=\"text-align: center\">\n",
    "<img src=\"../images/git/post-merge-push.svg\"/>\n",
    "<figcaption>clone2</figcaption>\n",
    "</figure>\n",
    "\n",
    "<figure style=\"text-align: center\">\n",
    "<img src=\"../images/git/post-merge-push-origin.svg\"/>\n",
    "<figcaption>central</figcaption>\n",
    "</figure>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "Zu Bemerken ist, dass hier `B` und `D` zwar *inhaltlich* identisch sind, aber da sich Metadaten und vor allem History unterscheiden, sind es trotzdem *unterschiedliche* Commits. `D` enthält neben dem Inhalt selbst die Zusatzinformation, dass hier entschieden wurde, beide Branches auf genau diese Weise zusammenzuführen.\n",
    "\n",
    "#### `pull`\n",
    "\n",
    "Die Kombination `git fetch` gefolgt von `git merge` ist so häufig, dass es dafür die Kurzform `git pull` gibt. Der `master`-Branch in `clone1` ist noch bei Commit `C`. Mit `pull` werden die Änderungen abgeholt:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Updating c8249b2..baef6b0\n",
      "Fast-forward\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "From /home/cruegge/central\n",
      "   c8249b2..baef6b0  master     -> origin/master\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone1\n",
    "git pull"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "Der Merge ist hier ein **Fast-forward**, d.h. der neue `master`-Branch ist ein Nachfolger des ursprünglichen `master`. Daher kann hier einfach die Ref umgesetzt werden, ohne einen Merge-Commit zu erzeugen.\n",
    "\n",
    "`clone1` sieht nun genauso aus wie `clone2` oben."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "commit baef6b0bbafdc5936b1a05f66a7524887ebe61c3\n",
      "Merge: 04179b3 c8249b2\n",
      "Author: Christoph Ruegge <c.ruegge@math.uni-goettingen.de>\n",
      "Date:   Tue Sep 13 20:55:10 2016 +0200\n",
      "\n",
      "    Fixed merge conflict\n",
      "\n",
      "commit 04179b3b2dbb367cf618f86caf6344a85c8a5c12\n",
      "Author: Christoph Ruegge <c.ruegge@math.uni-goettingen.de>\n",
      "Date:   Tue Sep 13 18:29:05 2016 +0200\n",
      "\n",
      "    Be less friendly.\n",
      "\n",
      "commit c8249b2c11c36fb882360f947f812f3d19281e93\n",
      "Author: Christoph Ruegge <c.ruegge@math.uni-goettingen.de>\n",
      "Date:   Tue Sep 13 18:22:34 2016 +0200\n",
      "\n",
      "    Be more friendly.\n",
      "\n",
      "commit 151a4280e3574fdba1c35d66b709998b01e4ee8b\n",
      "Author: Christoph Ruegge <c.ruegge@math.uni-goettingen.de>\n",
      "Date:   Tue Sep 13 17:13:17 2016 +0200\n",
      "\n",
      "    Initialer Commit: Hello world!\n"
     ]
    }
   ],
   "source": [
    "%%bash\n",
    "cd ~/clone1\n",
    "git log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "source": [
    "## Allgemeines\n",
    "\n",
    "### Weitere Befehle\n",
    "\n",
    "- **`git help <subcommand>`** gibt ausführliche aber oft sehr technische Hilfe zu allen Kommandos.\n",
    "\n",
    "- **`git diff`** zeigt die Änderungen im Worktree an, **`git diff --cached`** die Änderungen im Index.\n",
    "\n",
    "- Mit **`git branch`** kann man auch lokale Branches erzeugen (oben hatten wir nur Remote-Branches wie `origin/master`). Branches wechseln geht mit **`git checkout`**.\n",
    "\n",
    "- Mit **`git checkout`** kann man auch Dateien auf den gespeicherten Zustand im aktuellen Commit zurücksetzen.\n",
    "\n",
    "- \"Un-stagen\", also das Gegenteil von `git add`, ist **`git reset`**.\n",
    "\n",
    "- **`git config --global merge.conflictstyle diff3`** ist sehr empfehlenswert.\n",
    "\n",
    "### Hinweise\n",
    "\n",
    "- [git-scm.com](https://git-scm.com/) enthält weitere Informationen und sehr gute Dokumentation.\n",
    "\n",
    "- Graphische Frontends sind leider rar gesät. Für Windows ist [TortoiseGit](https://tortoisegit.org/) brauchbar, weitere Links, auch für Mac und Linux, gibt es auf [git-scm.com](https://git-scm.com/).\n",
    "\n",
    "- Linux-Nutzer mögen die Kommandozeile, Vim (Fugitive) oder Emacs (Magit).\n",
    "\n",
    "- Commits sollten klar definierte Änderungen enthalten. Schreibt gute Commit-Messages.\n",
    "\n",
    "[![](https://imgs.xkcd.com/comics/git_commit.png)](https://xkcd.com/1296/)"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
